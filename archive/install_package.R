# Install packages
source("https://bioconductor.org/biocLite.R")
biocLite("bioDist")
biocLite("Heatplus")
install.packages("gplots")
install.packages("heatmap.plus")
install.packages('RColorBrewer')
install.packages("readxl")
install.packages("dplyr")
